resource "datadog_monitor" "custom_aws_ec2_linux_windows_cpu" {
  name    = "[{{oworxawsaccid.name}}][{{host.name}}]CUSTOM_AWS_EC2_HighCPU"
  type    = "metric alert"
  message = <<-EOT
    {{#is_alert}}
    ;{{oworxcus.name}};{{last_triggered_at}};P2;{{host.name}};{{host.oworxawsaccname}};CUSTOM_AWS_EC2_HighCPU;
    Customer: {{oworxcus.name}}
    Priority: P2
    Description: AWS - EC2 - Linux&Windows - CPU --- Average utilisation high for over 1 hour - 95%
    Instance ID: {{host.name}}
    AWS Acct Name: {{oworxawsaccname.name}}
    Monitor last triggered: {{last_triggered_at}}
    Value: {{value}}
    @OworxMonitoringAlerts@scc.com
    {{/is_alert}}

    {{#is_warning}}
    ;{{oworxcus.name}};{{last_triggered_at}};P4;{{host.name}};{{host.oworxawsaccname}};CUSTOM_AWS_EC2_HighCPU;
    Customer: {{oworxcus.name}}
    Priority: P4
    Description: AWS - EC2 - Linux&Windows - CPU --- Average utilisation high for over 1 hour - 85%
    Instance ID: {{host.name}}
    AWS Acct Name: {{oworxawsaccname.name}}
    Monitor last triggered: {{last_triggered_at}}
    Value: {{value}}
    @OworxMonitoringAlerts@scc.com
    {{/is_warning}}
  EOT

  escalation_message = <<-EOT
    {{#is_alert}}
    ;{{oworxcus.name}};{{last_triggered_at}};P2;{{host.name}};{{host.oworxawsaccname}};CUSTOM_AWS_EC2_HighCPU;
    RE-ALERT!
    Customer: {{oworxcus.name}}
    Priority: P2
    Description: AWS - EC2 - Linux&Windows - CPU --- Average utilisation high for over 1 hour - 95%
    Instance ID: {{host.name}}
    AWS Acct Name: {{oworxawsaccname.name}}
    Monitor last triggered: {{last_triggered_at}}
    Value: {{value}}
    @OworxMonitoringAlerts@scc.com
    {{/is_alert}}

    {{#is_warning}}
    ;{{oworxcus.name}};{{last_triggered_at}};P4;{{host.name}};{{host.oworxawsaccname}};CUSTOM_AWS_EC2_HighCPU;
    RE-ALERT!
    Customer: {{oworxcus.name}}
    Priority: P4
    Description: AWS - EC2 - Linux&Windows - CPU --- Average utilisation high for over 1 hour - 85%
    Instance ID: {{host.name}}
    AWS Acct Name: {{oworxawsaccname.name}}
    Monitor last triggered: {{last_triggered_at}}
    Value: {{value}}
    @OworxMonitoringAlerts@scc.com
    {{/is_warning}}
  EOT

  query = "avg(last_1h):avg:aws.ec2.cpuutilization{oworxcloud:aws} by {oworxcus,oworxawsaccid,oworxawsaccname,instance_id,host,name} > 95"

  thresholds = {
    warning  = 85.0
    critical = 95.0
  }

  notify_no_data      = false
  no_data_timeframe   = null
  renotify_interval   = 1440
  require_full_window = true
  notify_audit        = true
  timeout_h           = 0
  new_host_delay      = 300
  evaluation_delay    = 900
  include_tags        = false

  # ignore any changes in silenced value; using silenced is deprecated in favor of downtimes
  lifecycle {
    ignore_changes = [silenced]
  }

  tags   = ["custom", "aws", "ec2", "highcpu", "integration", "bitbucket"]
  locked = true
}