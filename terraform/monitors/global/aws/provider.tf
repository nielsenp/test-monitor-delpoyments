variable "DD_API_KEY" {}
variable "DD_APP_KEY" {}

provider "datadog" {
  api_key = var.DD_API_KEY # Provided by pipeline env variables
  app_key = var.DD_APP_KEY # Provided by pipeline env variables
  api_url = "https://api.datadoghq.eu/"
}

terraform {
  required_version = ">=0.15"

  backend "s3" {
    bucket = "oworx-terraform"
    key    = "datadog-monitors/test-customer/global-monitors/aws/terraform.tfstate"
    region = "eu-west-2"
  }
  required_providers {
    aws = {
      version = ">= 3.20"
      source = "hashicorp/aws"
    }
    datadog = {
      source = "DataDog/datadog"
    }

  }
}