# Suppress High IOPS on DB Instance between 20:30 - 22:30
resource "datadog_downtime" "aws_rds_high_iops_instance_muted" {
  scope      = ["name:instance"]
  start_date = "2021-04-23T20:30:00Z"
  end_date   = "2021-04-23T22:30:00Z"

  active = true

  monitor_id = 912682 # AWS - RDS - High Read IOPs --- Read IOPs high for over 5 minutes
  message    = "Deployed via Terraform"

  recurrence {
    type   = "days"
    period = 1
  }

}